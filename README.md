# Windows CI Infrastructure

In this repo there is an ansible directory where ansible playbooks are stored and a packer directory where the packer image definitions are stored.

## Docker Image

We generate our own docker image to run Ansible and Packer in. This is defined in the `Dockerfile-ci`
and should generally only be run via CI itself.
