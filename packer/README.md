# Windows Packer Image

This sets up a Windows image for the CI managers. It builds a server and then
runs the ansible playbook to configure it and has 3 provisioning steps,

1. Run the PowerShell script described below
2. Run Ansible to configure the runner, security, and Windows updates
3. Reboot the machine to finish configuring Windows updates
4. Create image

## What does the PowerShell script do?

You'll notice there is a PowerShell script that runs before even ansible does.
This was taken mostly from the [Ansible documentation](https://docs.ansible.com/ansible/latest/user_guide/windows_winrm.html#certificate)
on how to connect to Windows.
It will take in a username and the password generated by Packer
in order to create a user for Ansible to connect as. It then takes that
user and sets up and enables certificate authentication for that user so
that we don't need to worry with passwords for Ansible authentication.

## Variables

All of the variables are currently set via environment variables for use in CI.
The options are documented below. The defaults can be found in the GitLab CI
configuration.

| Variable                   | Description                                                                                                |
| -------------------------- | ---------------------------------------------------------------------------------------------------------- |
| `GOOGLE_PROJECT_ID`        | The Google Project ID to build and put the image in.                                                       |
| `GOOGLE_COMPUTE_ZONE`      | The GCP zone to build the server to be imaged in.                                                          |
| `GOOGLE_COMPUTE_MACHINE`   | The machine size to build the image on.                                                                    |
| `VERSION`                  | Currently unused, but expected to be the version of the image.                                             |
| `WINDOWS_IMAGE_NAME`       | The name of the image. A version will be appended.                                                         |
| `ANSIBLE_USER`             | The username that Ansible will connect as.                                                                 |
| `ANSIBLE_USER_CERT`        | Base64 encoded public certificate to allow authentication to the ansible user.                             |
