$username = "$Env:ANSIBLE_USER"
$password = ConvertTo-SecureString -String "$Env:USER_PASSWORD" -AsPlainText -Force
$cert_bytes = [Convert]::FromBase64String("$Env:USER_CERT")

# Create local user

New-LocalUser -Name "$username" -Description "Automatically created administrator account" -Password $password
Add-LocalGroupMember -Group "Administrators" -Member "$username"

# enable cert Auth

Set-Item -Path WSMan:\localhost\Service\Auth\Certificate -Value $true

# import Cert Auth

$cert = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Certificate2
$cert.Import($cert_bytes)

$store_name = [System.Security.Cryptography.X509Certificates.StoreName]::Root
$store_location = [System.Security.Cryptography.X509Certificates.StoreLocation]::LocalMachine
$store = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Store -ArgumentList $store_name, $store_location
$store.Open("MaxAllowed")
$store.Add($cert)
$store.Close()

$store_name = [System.Security.Cryptography.X509Certificates.StoreName]::TrustedPeople
$store = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Store -ArgumentList $store_name, $store_location
$store.Open("MaxAllowed")
$store.Add($cert)
$store.Close()

# Associate cert Auth

$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $username, $password
$thumbprint = (Get-ChildItem -Path cert:\LocalMachine\root | Where-Object { $_.Subject -eq "CN=terraform" }).Thumbprint

New-Item -Path WSMan:\localhost\ClientCertificate `
    -Subject "terraform@localhost" `
    -URI * `
    -Issuer $thumbprint `
    -Credential $credential `
    -Force
