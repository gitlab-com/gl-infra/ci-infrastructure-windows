# Windows Updates Role

This will install windows updates. Currently it is planned to only
run this during packer provisioning so it does NOT reboot afterwards.
The reboot will be handled by the `windows-restart` provisioner
of packer.
