# GitLab Runner role

This role will install and configure the GitLab runner and autoscaling on a Windows server.

## Variables

There are several supported variables:

### Runner Variables

In order to support multiple runner definitions, the
runners are defined in a list. The defaults are as below.
Each item maps one to one with the actual config key names.

The `runner_global` section is for global config options
in `config.toml`

```yaml
runner_global:
  concurrent: 10
  listen_address: 127.0.0.1:9402

runners:
  - name: "windows-runner"
    global:
      request_concurrency: 10
      limit: 10
      token: ""
      url: "https://gitlab.com"
      executor: "custom"
      builds_dir: "C:\\GitLab-Runner\\builds"
      cache_dir: "C:\\GitLab-Runner\\cache"
      shell: "powershell"
    custom:
      config_exec: "C:\\GitLab-Runner\\autoscaler\\autoscaler.exe"
      config_args: ["--config", "C:\\GitLab-Runner\\autoscaler\\config.toml", "custom", "config"]
      prepare_exec: "C:\\GitLab-Runner\\autoscaler\\autoscaler.exe"
      prepare_args: ["--config", "C:\\GitLab-Runner\\autoscaler\\config.toml", "custom", "prepare"]
      run_exec: "C:\\GitLab-Runner\\autoscaler\\autoscaler.exe"
      run_args: ["--config", "C:\\GitLab-Runner\\autoscaler\\config.toml", "custom", "run"]
      cleanup_exec: "C:\\GitLab-Runner\\autoscaler\\autoscaler.exe"
      cleanup_args: ["--config", "C:\\GitLab-Runner\\autoscaler\\config.toml", "custom", "cleanup"]
```

### Autoscaler Variables

| Variable                             | Description                                                                            | Default                                                         |
| ------------------------------------ | -------------------------------------------------------------------------------------- | --------------------------------------------------------------- |
| `runner_autoscaler_disk_size`        | The size of the boot volume for the autoscaled machines                                | `50`                                                            |
| `runner_autoscaler_disk_type`        | Type of disk for the boot disk                                                         | `pd-standard`                                                   |
| `runner_autoscaler_image`            | The image autoscaled machines will be built with                                       | `global/images/runners-windows-2019-core-containers-01-24-2020` |
| `runner_autoscaler_machine_type`     | Server size the autoscaled machines will use                                           | `n1-standard-2`                                                 |
| `runner_autoscaler_project`          | The GCP project name the autoscaled servers will be built in                           | ""                                                              |
| `runner_autoscaler_zone`             | The GCP zone that autoscaled servers will be built in                                  | `us-east1-c`                                                    |
| `runner_autoscaler_service_account`  | Path to the GCP service account credentials                                            |                                                                 |
| `runner_autoscaler_network`          | Network for autoscaled machines                                                        | `default`                                                       |
| `runner_autoscaler_subnet`           | Subnet for autoscaled machines                                                         | `default`                                                       |
| `runner_autoscaler_tag`              | The tag to be in the name of autoscaled machines, ex: `runner-<token>-<tag>-otherdata` | `windows`                                                       |
| `runner_autoscaler_vm_tag`           | Tag that the specific runner machines should be assigned                               | `windows-runner`                                                |
| `runner_autoscaler_user`             | Windows user the runner will connect as                                                | `gitlab_runner`                                                 |
| `runner_autoscaler_internal_ip_only` | Build executor machines with a public IP                                               | `true`                                                          |
