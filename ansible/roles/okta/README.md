# Windows Security Role

This ansible role sets up security defaults and installs Okta ASA.

## Variables

| Variable                | Description                                                                                    | Default  |
| ----------------------- | ---------------------------------------------------------------------------------------------- | -------- |
| `scaleft_tools_version` | The [version](https://dist.scaleft.com/server-tools/windows/) of the scaleft tools to install. | `latest` |
