# Windows CI Ansible

This is the ansible directory for roles and playbooks related to the Windows CI infrastructure.

All auth to GCP is done via the runner's associated service account.
This will allow us to more easily integrate with GitLab CI and the [file variable type](
https://docs.gitlab.com/ee/ci/variables/README.html#file-type,
).

As this is currently set up, the GCE instance will need to have the labels
`os = windows` and `role = runner_manager`.
